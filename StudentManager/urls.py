from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from views import Calculator, StudentManager


urlpatterns = [
   url(r'^sum/$', csrf_exempt(Calculator.as_view()),
       name="Assignment-sum"),
   url(r'^list/$', csrf_exempt(StudentManager.as_view()),
       name="Assignment-student_list"),
   url(r'^edit/(?P<id>\d+)/$', csrf_exempt(StudentManager.as_view()),
       name="Assignment-student_delete"),
   url(r'^update/(?P<roll>\d+)/$', csrf_exempt(StudentManager.as_view()),
       name="Assignment-student_edit")
]
