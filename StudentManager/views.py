from django.shortcuts import render
from django.views.generic import View
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import ObjectDoesNotExist
from django.http import QueryDict
from django.http import HttpResponse
from django.db import IntegrityError
from .models import Student

# Class performs basic arithmetic functions


class Calculator(View):

    def get(self, request):
        try:
            p1 = request.GET['p1']
            p2 = request.GET['p2']
            r1 = int(p1) + int(p2)
            r2 = int(p1) - int(p2)
            r3 = int(p1) * int(p2)
            r4 = int(p1) / float(p2)
            return HttpResponse('sum is : ' + str(r1) + ' diff. is : ' + str(r2) + ' product is : ' + str(r3) +
                                ' quotient is : ' + str(r4), status=200)

        except Exception as exception:
            str(exception)
            return HttpResponse("Internal Server Error", status=500)


# Class performs basic functions like display, add, delete and modify data for Student Database


class StudentManager(View):

    # Method to display Student data

    def get(self,request):
        try:
            student_list = Student.objects.all()
            result = []
            for student in student_list:
                res = {}
                res['first_name'] = student.first_name
                res['last_name'] = student.last_name
                res['college_name'] = student.college_name
                res['roll_no'] = student.roll_no
                res['email'] = student.email
                result.append(res)
            return HttpResponse(result, status=200)

        except Exception as exception:
            str(exception)
            return HttpResponse('Internal Server Error', status=500)

    # Method for creating an object in the Student database

    def post(self, request):
        try:
            post_req = request.POST

            f_name_ = post_req['first_name']
            l_name_ = post_req['last_name']
            college_ = post_req['college_name']
            email_ = post_req['email']
            rno_ = post_req['roll_no']

            obj = Student(first_name=f_name_, last_name=l_name_, college_name=college_, email=email_, roll_no=rno_)
            obj.save()
            return HttpResponse(str(f_name_) + ' ' + str(rno_) + " record  successfully created ", status=200)
        except IntegrityError:
            return HttpResponse("Roll No already exists!")
        except MultiValueDictKeyError:
            return HttpResponse("Multiple values returned in the dictionary", status=500)

    # Method to delete a record from the database using auto-created field id

    def delete(self, request, id):
        try:
            Student.objects.get(id=id).delete()
            return HttpResponse("Object deleted successfully", status=200)

        except ObjectDoesNotExist:
            return HttpResponse('Object does not exist', status=500)

        except MultiValueDictKeyError:
            return HttpResponse('Multiple values returned in the dictionary', status=500)

    # Method to edit an existing record by using roll number

    def put(self, request, roll):
        try:
            put_req = QueryDict(request.body)
            obj = Student.objects.get(roll_no=roll)

            if 'first_name' in put_req.keys():
                obj.first_name = put_req['first_name']
            if 'last_name' in put_req.keys():
                obj.last_name = put_req['last_name']
            if 'email' in put_req.keys():
                obj.email = put_req['email']
            if 'college_name' in put_req.keys():
                obj.college_name = put_req['college_name']

            obj.save()
            return HttpResponse("roll no : " + str(roll) + "successfully Updated", status=200)
        except ObjectDoesNotExist:
            return HttpResponse('Object does not exist', status=500)
