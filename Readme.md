#This contains the description of the app StudentManager in the site Assignment

The app Lists, Inserts, Updates and Deletes the student record in Student model (/database) using 
GET, POST, PUT and DELETE request respectively by employing the use of generice views in Django framework. Also, a basic calculator is also implemented.

##Getting Started

Cloning a repository to clone the repository in your local system run git clone https://muditgrwl_happay@bitbucket.org/muditgrwl_happay/assignment1.git

##Prerequisites

- Djnago 1.8
- sqlite (available in the Django framework) 

##Installing django

pip install django==1.8

##Running the tests

*make sure to run the command: python manage.py makemigrations to check for any changes in models*
*to apply any migrations run the command: python manage.py migrate to apply any migrations*

1. Run the command - python manage.py runserver 
2. Go to adress http://127.0.0.1:8000/admin/ to access admin
   1. username- MUDIT
   2. password -666seals
3. To Create an entry in the database hit the POST Request at http://127.0.0.1:8000/student/list/ in the 	    POSTMAN client with body parameters as 'first_name', 'last_name', 'roll_no', 'college_name'and 'email'
4. To List the entries in the database hit the GET Request at http://127.0.0.1:8000/student/list/ in the        browser
5. To Update an entry in the database hit the PUT Request at http://127.0.0.1:8000/student/update/x/ in the      POSTMAN client
   {x represents roll number you wish to update and fill the details in body of request with keys first_name,     last_name, college_name, email}
6. To delete the detail hit the DELETE Request at http://127.0.0.1:8000/student/edit/x/ in the POSTMAN client
   {x represents the ID number of the record you wish to delete}
7. To use the calculator go to address http://127.0.0.1:8000/student/sum/?p1=x&p2=y                     
   {x and y represent integers you wish to operate on}
8. The calculator can also be accessed using http://127.0.0.1:8000/student/sum/ in the POSTMAN client with      body parameters as p1 and p2
